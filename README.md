# README #



### What is this repository for? ###

This project is an attempt to fit analog musical devices by neural networks.

### How do I get set up? ###

For the project you need the following libraries:

1) FANN

2) libsndfile (until I will build in only the WAV reading/writing part)

3) compile the libraries

4) set up directories like in the file 'TDRNN/make.ink'

5) type 'make' in TDRNN/ (and in tests/*/ directories to compile the tests)


### Who do I talk to? ###

PechenegFX
//
//  Tdrnn_Train.cpp
//  test_fann
//
//  Created by Ilia Sivkov on 17/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#include <stdio.h>
#include <doublefann.h>
#include <stdexcept>
#include <iostream>

#include "TdrnnTrain.h"
#include "AnnWrapRProp.h"

namespace fann_pfx
{
TdrnnTrain::TdrnnTrain(Tdrnn* tdrnn, AudioTrainData* trainData):
			_tdrnn( tdrnn ),
			_trainData( trainData )
{
	if( !_trainData->isLoaded() )
	{
		std::runtime_error("AudioTrainData is not loaded");
	}
}

//----------------------------------------------------------------------------
// copied from parallel fersion (pfx FANN) with changes.
// Train for all epochs
//----------------------------------------------------------------------------
void TdrnnTrain::train(unsigned int max_epochs,
		unsigned int epochs_between_reports,
		float desired_error)
{
	float error;
	unsigned int i;
	int desired_error_reached;


#ifdef DEBUG
	printf("Training with %s\n", FANN_TRAIN_NAMES[ann->training_algorithm]);
#endif

	fann* ann = _tdrnn->getANN();

	if(epochs_between_reports && ann->callback == NULL)
	{
		printf("Max epochs %8d. Desired error: %.10f.\n", max_epochs, desired_error);
	}

	for(i = 1; i <= max_epochs; i++)
	{
		/*
		 * train
		 */
		error = trainEpoch();

		desired_error_reached = fann_desired_error_reached(ann, desired_error);

		/*
		 * print current output
		 */
		if(epochs_between_reports &&
				(i % epochs_between_reports == 0 || i == max_epochs || i == 1 ||
						desired_error_reached == 0))
		{
			printf("Epochs     %8d. MSE: %.10f. Desired-MSE: %.5f  Bit fail %d.\n", i, fann_get_MSE(ann), desired_error,
					ann->num_bit_fail);
		}

		if(desired_error_reached == 0)
			break;
	}
}


//----------------------------------------------------------------------------
void TdrnnTrain::train(unsigned int max_epochs,
				unsigned int epochs_between_reports,
				float desired_error,
				std::vector<double>& procData,
				std::vector<double>& cleanData,
				int start, int end,
				std::vector<double> controlParams)
{
	float error;
	unsigned int i;
	int desired_error_reached;


	#ifdef DEBUG
	printf("Training with %s\n", FANN_TRAIN_NAMES[ann->training_algorithm]);
	#endif

	fann* ann = _tdrnn->getANN();

	if(epochs_between_reports && ann->callback == NULL)
	{
		printf("Max epochs %8d. Desired error: %.10f.\n", max_epochs, desired_error);
	}

	for(i = 1; i <= max_epochs; i++)
	{
		/*
		 * train
		 */
		error = trainEpochOnExtData(procData,
				cleanData,
				start,end,
				controlParams);

		desired_error_reached = fann_desired_error_reached(ann, desired_error);

		/*
		 * print current output
		 */
		if(epochs_between_reports &&
				(i % epochs_between_reports == 0 || i == max_epochs || i == 1 ||
						desired_error_reached == 0))
		{
			printf("Epochs     %8d. MSE: %.10f. Desired-MSE: %.5f  Bit fail %d.\n", i, fann_get_MSE(ann), desired_error,
					ann->num_bit_fail);
		}

		if(desired_error_reached == 0)
			break;
	}
}


//----------------------------------------------------------------------------
void TdrnnTrain::train(unsigned int max_epochs,
		unsigned int epochs_between_reports,
		float desired_error,
		std::vector<double>& procData,
		std::vector<double>& cleanData)
{
	train(max_epochs,
			epochs_between_reports,
			desired_error,
			procData,
			cleanData,
			0, procData.size());
}


//----------------------------------------------------------------------------
// train NN for one epoch with audio data
//----------------------------------------------------------------------------
double TdrnnTrain::trainEpoch()
{
	// prepare train data
	std::vector<double> input( _tdrnn->getFullNumInputs() );

	int num_td_inputs   = _tdrnn->getNumTimeDelayedInputs();
	int num_tdr_outputs = _tdrnn->getNumTimeDelayedRecurrentOutputs();

	int num_tot_td = num_tdr_outputs + num_td_inputs;

	int num_td = std::max(num_td_inputs, num_tdr_outputs + 1);

	const std::vector<double>& clean_signal = _trainData->getCleanSignal();

	const std::vector<train_signal_data_t>& train_samples = _trainData->getTrainSamples();

	std::vector<train_data_piece_info_t> piecesInfo = _trainData->getPiecesInfo();

	fann* ann = _tdrnn->getANN();

	AnnWrapRProp annw(ann);

	annw.prepare();

	// loop over pieces, OMP
#pragma omp parallel for reduction( + : annw) firstprivate(input) schedule(static)
	for( int ipiece=0; ipiece < piecesInfo.size(); ipiece++ )
	{
		auto& piece = piecesInfo[ ipiece ] ;

		auto& signal = train_samples[ piece.signalIdx ];

		int start_frame_num = piece.dataOffset;

		// if we have 0th piece we should start not from the beginning
		// because we need to have time-delayed frames from the past
		if ( piece.dataOffset == 0 )
		{
			start_frame_num = num_td;
		}

		// set control params in input array
		for(int icp = 0; icp < signal.control_params.size(); ++icp )
		{
			input[num_tot_td + icp] = signal.control_params[icp];
		}

		const std::vector<double>& signalData = signal.data;

		// loop over frames
		for(int i = start_frame_num ; i < piece.size + piece.dataOffset - _forwardShift; i++)
		{

			// fill input vector of ANN
//			for(int iinp=0; iinp < num_td_inputs; ++iinp)
//			{
//				input[iinp] = clean_signal[i - num_td_inputs + iinp];
//			}
//
//			for(int iinp = 0; iinp < num_tdr_outputs; ++iinp)
//			{
//				input[iinp + num_td_inputs] = signal_data[i - num_tdr_outputs + iinp];
//			}

			memcpy(&input[0], &clean_signal[i - num_td_inputs + _forwardShift], sizeof(double) * num_td_inputs );

			memcpy(&input[num_td_inputs], &signalData[i - num_tdr_outputs], sizeof(double) * num_tdr_outputs );

			double outp = signalData[i];

//							#pragma omp critical
//							{
//								for(int iinp=0; iinp < num_td_inputs; ++iinp)
//								{
//									std::cout<< clean_signal[i - num_td_inputs + iinp] <<" ";
//								}
//								std::cout<<std::endl;
//								for(int iinp = 0; iinp < num_tdr_outputs; ++iinp)
//								{
//									std::cout<< sample.signal_data[i - num_tdr_outputs + iinp] <<" ";
//								}
//								std::cout<<std::endl;
//								for(auto a: input) std::cout<<a<<"   ";
//								std::cout<<"| "<<sample.signal_data[i];
//								std::cout<<"\n------------"<<std::endl;
//								std::cin.get();
//							}

			annw.trainIteration(input.data(), &outp);
		}
	}

	annw.updateWeights();

	return _tdrnn->get_MSE();
}


//----------------------------------------------------------------------------
double TdrnnTrain::trainEpochOnExtData(std::vector<double>& procData,
		std::vector<double>& cleanData,
		int start, int end,
		std::vector<double> controlParams)
{
	fann* ann = _tdrnn->getANN();

	AnnTrainWrap annw(ann);

	std::vector<double> input(_tdrnn->getFullNumInputs());

	int num_td_inputs   = _tdrnn->getNumTimeDelayedInputs();
	int num_tdr_outputs = _tdrnn->getNumTimeDelayedRecurrentOutputs();

	int num_tot_td = num_tdr_outputs + num_td_inputs;

	int num_td = std::max(num_td_inputs, num_tdr_outputs + 1);

	// set control params in input array
	for(int icp = 0; icp < _tdrnn->getNumControlParams(); ++icp )
	{
		input[num_tot_td + icp] = controlParams[icp];
	}

	if(ann->prev_train_slopes == NULL)
	{
		fann_clear_train_arrays(ann);
	}

	fann_reset_MSE(ann);

	int start_frame_num = std::max(start, num_td);
	int end_frame_num = std::min(end, (int)procData.size());

	// loop over frames
#pragma omp parallel for reduction( + : annw) firstprivate(input) schedule(static)
	for(int i = start_frame_num ; i < end_frame_num - _forwardShift; i++)
	{
		fann* loc_ann = annw.getANN();

		memcpy(&input[0], &cleanData[i - num_td_inputs + _forwardShift], sizeof(double) * num_td_inputs );

		memcpy(&input[num_td_inputs], &procData[i - num_tdr_outputs], sizeof(double) * num_tdr_outputs );

//		for(int iinp=0; iinp < num_td_inputs; ++iinp)
//		{
//			input[iinp] = cleanData[i - num_td_inputs + iinp];
//		}

//		for(int iinp = 0; iinp < num_tdr_outputs; ++iinp)
//		{
//			input[iinp + num_td_inputs] = procData[i - num_tdr_outputs + iinp];
//		}

		double *outp = &procData[i];

		fann_run(loc_ann, input.data());
		fann_compute_MSE(loc_ann, outp );
		fann_backpropagate_MSE(loc_ann);
		fann_update_slopes_batch(loc_ann, loc_ann->first_layer + 1, loc_ann->last_layer - 1);
	}

	fann_update_weights_irpropm(ann, 0, ann->total_connections);
}


//----------------------------------------------------------------------------
double TdrnnTrain::trainEpochOnExtData(std::vector<double>& procData,
		std::vector<double>& cleanData)
{
	trainEpochOnExtData(procData, cleanData, 0, procData.size());
}


void TdrnnTrain::setForwardShift(int forwardShift)
{
	_forwardShift = forwardShift;
}


//----------------------------------------------------------------------------
// copied from FANN with changes - dataSpread should be known before or default
//----------------------------------------------------------------------------
void TdrnnTrain::initWeights(double dataSpread)
{
	fann* ann = _tdrnn->getANN();

	unsigned int dat = 0, elem, num_connect, num_hidden_neurons;
	struct fann_layer *layer_it;
	struct fann_neuron *neuron_it, *last_neuron, *bias_neuron;

#ifdef FIXEDFANN
	unsigned int multiplier = ann->multiplier;
#endif

	float scale_factor;

	num_hidden_neurons =
			ann->total_neurons - (ann->num_input + ann->num_output +
					(ann->last_layer - ann->first_layer));
	scale_factor =
			(float) (pow
					((double) (0.7f * (double) num_hidden_neurons),
							(double) (1.0f / (double) ann->num_input)) / dataSpread);

#ifdef DEBUG
	printf("Initializing weights with scale factor %f\n", scale_factor);
#endif

	bias_neuron = ann->first_layer->last_neuron - 1;
	for(layer_it = ann->first_layer + 1; layer_it != ann->last_layer; layer_it++)
	{
		last_neuron = layer_it->last_neuron;

		if(ann->network_type == FANN_NETTYPE_LAYER)
		{
			bias_neuron = (layer_it - 1)->last_neuron - 1;
		}

		for(neuron_it = layer_it->first_neuron; neuron_it != last_neuron; neuron_it++)
		{
			for(num_connect = neuron_it->first_con; num_connect < neuron_it->last_con;
					num_connect++)
			{
				if(bias_neuron == ann->connections[num_connect])
				{
#ifdef FIXEDFANN
					ann->weights[num_connect] =
							(fann_type) fann_rand(-scale_factor, scale_factor * multiplier);
#else
					ann->weights[num_connect] = (fann_type) fann_rand(-scale_factor, scale_factor);
#endif
				}
				else
				{
#ifdef FIXEDFANN
					ann->weights[num_connect] = (fann_type) fann_rand(0, scale_factor * multiplier);
#else
					ann->weights[num_connect] = (fann_type) fann_rand(0, scale_factor);
#endif
				}
			}
		}
	}

#ifndef FIXEDFANN
	if(ann->prev_train_slopes != NULL)
	{
		fann_clear_train_arrays(ann);
	}
#endif
}

}

LIBSNDDIR=/Users/isivkov/Projects/audio_libs/libsndfile-1.0.27
FANNDIR=/Users/isivkov/Projects/ANN/FANN-2.2.0-Source

CXX = g++

OPTS_RLS = -O3 -std=c++11 -fopenmp
OPTS_DBG = -O0 -g -ggdb -std=c++11 -fopenmp

OPTS := $(OPTS_RLS)

### libraries ###########
TDRNNLIBS = $(LIBSNDDIR)/src/.libs/libsndfile.a $(FANNDIR)/src/libdoublefann.a

COMMON_LIBS = -L/opt/local/lib  -lm -lFLAC -logg -lvorbis -lvorbisenc

TDRNNLIBS := $(TDRNNLIBS) $(COMMON_LIBS)

### includes ###########
TDRNNINCLUDE = -I$(FANNDIR)/src/include -I$(LIBSNDDIR)/src/

//
//  fann_train_pfx.cpp
//  test_fann
//
//  Created by Ilia Sivkov on 13/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#include <memory>
#include <iostream>
#include <vector>
#include "fann_train_pfx.h"
#include "AnnTrainWrap.h"
// this file is parallel version of fann train
// the code will be added step by step as it will be needed


namespace fann_pfx
{
	// debug function
	void fann_print_weights(fann *ann)
	{

		for(int i=0;i<fann_get_total_connections(ann); i++)
		{
			std::cout<<ann->weights[i]<<" ";
		}
		std::cout<<std::endl;
	}


	
	
	//----------------------------------------------
	// parallel IRPROP train func
	//----------------------------------------------
	float fann_train_epoch_irpropm_parallel(struct fann *ann, struct fann_train_data *data)
	{
		unsigned int i;
		
		if(ann->prev_train_slopes == NULL)
		{
			fann_clear_train_arrays(ann);
		}
		
		fann_reset_MSE(ann);
		
		AnnTrainWrap annw(ann);

        #pragma omp parallel for reduction( + : annw)
		for(i = 0; i < data->num_data; i++)
		{
			fann* ann = annw.getANN();
			fann_run(ann, data->input[i]);
			fann_compute_MSE(ann, data->output[i]);
			fann_backpropagate_MSE(ann);
			fann_update_slopes_batch(ann, ann->first_layer + 1, ann->last_layer - 1);
		}

		fann_update_weights_irpropm(ann, 0, ann->total_connections);
		
		return fann_get_MSE(ann);
	}
	
	
	
	//----------------------------------------------
	//----------------------------------------------
	float fann_train_epoch_parallel(struct fann *ann, struct fann_train_data *data)
	{
		if(fann_check_input_output_sizes(ann, data) == -1)
			return 0;
		
		switch (ann->training_algorithm)
		{
			case FANN_TRAIN_QUICKPROP:
				return fann_train_epoch_quickprop(ann, data);
			case FANN_TRAIN_RPROP:
				return fann_train_epoch_irpropm_parallel(ann, data);
			case FANN_TRAIN_SARPROP:
				return fann_train_epoch_sarprop(ann, data);
			case FANN_TRAIN_BATCH:
				return fann_train_epoch_batch(ann, data);
			case FANN_TRAIN_INCREMENTAL:
				return fann_train_epoch_incremental(ann, data);
		}
		return 0;
	}
	
	
	
	//----------------------------------------------
	//----------------------------------------------
	void fann_train_on_data_parallel(struct fann *ann, struct fann_train_data *data,
												   unsigned int max_epochs,
												   unsigned int epochs_between_reports,
												   float desired_error)
	{
		float error;
		unsigned int i;
		int desired_error_reached;
		
		#ifdef DEBUG
		printf("Training with %s\n", FANN_TRAIN_NAMES[ann->training_algorithm]);
		#endif
		
		if(epochs_between_reports && ann->callback == NULL)
		{
			printf("Max epochs %8d. Desired error: %.10f.\n", max_epochs, desired_error);
		}
		
		for(i = 1; i <= max_epochs; i++)
		{
			/*
			 * train
			 */
			error = fann_train_epoch_parallel(ann, data);
			
			desired_error_reached = fann_desired_error_reached(ann, desired_error);
			
			/*
			 * print current output
			 */
			if(epochs_between_reports &&
			   (i % epochs_between_reports == 0 || i == max_epochs || i == 1 ||
				desired_error_reached == 0))
			{
				if(ann->callback == NULL)
				{
					printf("Epochs     %8d. Current error: %.10f. Bit fail %d.\n", i, error,
						   ann->num_bit_fail);
				}
				else if(((*ann->callback)(ann, data, max_epochs, epochs_between_reports,
										  desired_error, i)) == -1)
				{
					/*
					 * you can break the training by returning -1
					 */
					break;
				}
			}
			
			if(desired_error_reached == 0)
				break;
		}
	}
	
}

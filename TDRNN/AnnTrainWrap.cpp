/*
 * AnnWrap.cpp
 *
 *  Created on: Jan 5, 2017
 *      Author: isivkov
 */

#include "AnnTrainWrap.h"


namespace fann_pfx
{

//-------------------------------------------------------
AnnTrainWrap::AnnTrainWrap(fann* ann)
{
	wrap(ann);
}

//-------------------------------------------------------
AnnTrainWrap::~AnnTrainWrap()
{
	if (_isCopied)
	{
		fann_destroy(_ann);
	}
}
//-------------------------------------------------------
void AnnTrainWrap::wrap(fann* ann)
{
	_ann = ann;
}

//-------------------------------------------------------
void AnnTrainWrap::prepare()
{
	if(_ann->prev_train_slopes == NULL)
	{
		fann_clear_train_arrays(_ann);
	}

	fann_reset_MSE(_ann);
}

//-------------------------------------------------------
void AnnTrainWrap::copyFrom(AnnTrainWrap* annw)
{
	if (_isCopied)
	{
		fann_destroy(_ann);
	}
	_ann = fann_copy(annw->getANN());
	_isCopied = true;
}

//-------------------------------------------------------
void AnnTrainWrap::mergeWith(AnnTrainWrap* annw)
{
	for (int i = 0; i < _ann->total_connections; i++)
	{
		_ann->train_slopes[i] += annw->getANN()->train_slopes[i];
	}

	_ann->num_MSE += annw->getANN()->num_MSE;
	_ann->MSE_value += annw->getANN()->MSE_value;
	_ann->num_bit_fail += annw->getANN()->num_bit_fail;
}

//-------------------------------------------------------
fann* AnnTrainWrap::getANN()
{
	return _ann;
}


//-------------------------------------------------------
void AnnTrainWrap::setANN(fann* ann)
{
	if( _isCopied )
	{
		fann_destroy(_ann);
	}
	_ann = ann;
}




}

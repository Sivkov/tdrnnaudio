/*
 * Ann.cpp
 *
 *  Created on: Jan 19, 2017
 *      Author: isivkov
 */



#include <stdexcept>
#include <algorithm>

#include "Ann.h"

namespace fann_pfx
{


Ann::Ann(std::initializer_list<unsigned int> layersSizes)
{
	//_layerSizes = std::vector<unsigned int>(_hidden_layers_sizes);

	create(std::vector<unsigned int>(layersSizes));
	//ann = fann_create_standard_array(static_cast<unsigned int>(_layerSizes.size()), _layerSizes.data());
};


Ann::Ann(std::vector<unsigned int> layersSizes, std::vector<LayerConnectionInfo> layersConnectionInfo)
{
	create(layersSizes, layersConnectionInfo);
}

Ann::Ann(std::string fname)
{
	create(fname);
}

Ann::~Ann()
{
	fann_destroy(_ann);
}

//--------------------------------------------------------------------------------
// own  methods
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
fann* Ann::getANN()
{
	return _ann;
}

void Ann::create( std::vector<unsigned int> layerSizes )
{
	_ann = fann_create_standard_array(static_cast<unsigned int>(layerSizes.size()), layerSizes.data());
}

void Ann::create( std::string fname )
{
	_ann = fann_create_from_file( fname.c_str() );
}

void Ann::allocateBase( std::vector<unsigned int> layersSizes )
{
	//-------------------------
	// FANN code
	//-------------------------
	struct fann_layer *layer_it, *last_layer, *prev_layer;
	struct fann_neuron *neuron_it, *last_neuron, *random_neuron, *bias_neuron;

	_ann = fann_allocate_structure(layersSizes.size());

	if( _ann == NULL )
	{
		fann_error(NULL, FANN_E_CANT_ALLOCATE_MEM);
		std::runtime_error( "Error in Ann::allocateBase, fann_allocate_structure");
	}

	// just a stub
	_ann->connection_rate = 0.5;

	/* determine how many neurons there should be in each layer */
	int i = 0;
	for(layer_it = _ann->first_layer; layer_it != _ann->last_layer; layer_it++)
	{
		/* we do not allocate room here, but we make sure that
		 * last_neuron - first_neuron is the number of neurons */
		layer_it->first_neuron = NULL;
		layer_it->last_neuron = layer_it->first_neuron + layersSizes[i++] + 1;	/* +1 for bias */
		_ann->total_neurons += layer_it->last_neuron - layer_it->first_neuron;
	}

	_ann->num_output = (_ann->last_layer - 1)->last_neuron - (_ann->last_layer - 1)->first_neuron - 1;
	_ann->num_input = _ann->first_layer->last_neuron - _ann->first_layer->first_neuron - 1;

	/* allocate room for the actual neurons */
	fann_allocate_neurons(_ann);
	if(_ann->errno_f == FANN_E_CANT_ALLOCATE_MEM)
	{
		fann_destroy(_ann);
		std::runtime_error( "Error in Ann::allocateBase, fann_allocate_neurons");
	}
}


void Ann::create(std::vector<unsigned int> layersSizes, std::vector<LayerConnectionInfo> layersConnectionInfo)
{
	allocateBase( layersSizes );

	// layersConnectionInfo.size() should be number of layer - 1, because 1st layer has no connections
	if( layersConnectionInfo.size() != layersSizes.size() - 1 )
	{
		std::runtime_error( "Error: wrong connection-info size");
	}

	std::vector<struct fann_neuron*> tmpConnections;

	// this hack is needed for reducing code repeatness
	unsigned int connectionNumber = iterateOverConnections( layersSizes, layersConnectionInfo,
	[&](int toLayerIdx, int toNodeIdx, int fromNodeIdx, int currentConIdx)
	{
		tmpConnections.push_back(&_ann->first_layer[toLayerIdx-1].first_neuron[fromNodeIdx]);
	},
	[&](int toLayerIdx, int toNodeIdx, int firstCon, int lastCon)
	{
		_ann->first_layer[toLayerIdx].first_neuron[toNodeIdx].activation_function  = layersConnectionInfo[toLayerIdx-1].activationFuncType;
		_ann->first_layer[toLayerIdx].first_neuron[toNodeIdx].activation_steepness = layersConnectionInfo[toLayerIdx-1].activationStepness;
		_ann->first_layer[toLayerIdx].first_neuron[toNodeIdx].first_con = firstCon;
		_ann->first_layer[toLayerIdx].first_neuron[toNodeIdx].last_con = lastCon;
	});

	_ann->total_connections = connectionNumber;

	if( tmpConnections.size() != connectionNumber )
	{
		std::runtime_error( "Error in Ann::create, calculated number of connections does not coincide with the length of the connections array");
	}

	// allocate connections
	fann_allocate_connections(_ann);
	if(_ann->errno_f == FANN_E_CANT_ALLOCATE_MEM)
	{
		fann_destroy(_ann);
		std::runtime_error( "Error in Ann::create, fann_allocate_connections");
	}

	// copy connections (pointers to nodes where connections start) and generate random weights
	for( int i = 0; i < connectionNumber; i++ )
	{
		_ann->connections[i] = tmpConnections[i];
		_ann->weights[i] = (fann_type) fann_random_bias_weight();
	}

}

//--------------------------------------------------------------------------------
// These methods are wrappers for FANN functions
//--------------------------------------------------------------------------------
void Ann::set_activation_steepness_hidden(fann_type steepness)
{
	fann_set_activation_steepness_hidden(_ann, steepness);
}

void Ann::set_activation_steepness_output(fann_type steepness)
{
	fann_set_activation_steepness_output(_ann, steepness);
}

void Ann::set_activation_function_hidden(fann_activationfunc_enum activation_function)
{
	fann_set_activation_function_hidden(_ann, activation_function);
}

//--------------------------------------------------------------------------------
void Ann::set_activation_function_output(fann_activationfunc_enum activation_function)
{
	fann_set_activation_function_output(_ann, activation_function);
}


//--------------------------------------------------------------------------------
void Ann::set_train_stop_function(fann_stopfunc_enum train_stop_function)
{
	fann_set_train_stop_function(_ann, train_stop_function);
}


//--------------------------------------------------------------------------------
void Ann::set_bit_fail_limit(fann_type bit_fail_limit)
{
	fann_set_bit_fail_limit(_ann, bit_fail_limit);
}


//--------------------------------------------------------------------------------
void Ann::set_training_algorithm(fann_train_enum training_algorithm)
{
	fann_set_training_algorithm(_ann, training_algorithm);
}


//--------------------------------------------------------------------------------
void Ann::init_weights(fann_train_data* data)
{
	fann_init_weights(_ann, data);
}

//--------------------------------------------------------------------------------
void Ann::print_connections()
{
	fann_print_connections(_ann);
}

//--------------------------------------------------------------------------------
void Ann::train_on_data(fann_train_data* data,
		unsigned int max_epochs,
		unsigned int epochs_between_reports,
		fann_type desired_error)
{
	fann_train_on_data(_ann, data, max_epochs, epochs_between_reports, desired_error);
}


//--------------------------------------------------------------------------------
void Ann::train_on_data_parallel(fann_train_data* data,
		unsigned int max_epochs,
		unsigned int epochs_between_reports,
		fann_type desired_error)
{
	fann_train_on_data_parallel(_ann, data, max_epochs, epochs_between_reports, desired_error);
}

//--------------------------------------------------------------------------------
fann_type* Ann::run(fann_type* input)
{
	return fann_run(_ann, input);
}


//--------------------------------------------------------------------------------
float Ann::test_data(fann_train_data* data)
{
	return fann_test_data(_ann, data);
}


//--------------------------------------------------------------------------------
int Ann::save(const char *configuration_file)
{
	return fann_save(_ann, configuration_file);
}

double Ann::get_MSE()
{
	return (double)fann_get_MSE(_ann);
}


}

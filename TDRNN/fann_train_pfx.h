//
//  fann_train_pfx.hpp
//  test_fann
//
//  Created by Ilia Sivkov on 13/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#ifndef fann_train_pfx_h
#define fann_train_pfx_h

// this file is parallel version of fann train
// the code will be added step by step as it will be needed

#include <stdio.h>

extern "C"
{
#include <doublefann.h>

float fann_train_epoch_quickprop(struct fann *ann, struct fann_train_data *data);

float fann_train_epoch_irpropm(struct fann *ann, struct fann_train_data *data);

float fann_train_epoch_sarprop(struct fann *ann, struct fann_train_data *data);

float fann_train_epoch_batch(struct fann *ann, struct fann_train_data *data);

float fann_train_epoch_incremental(struct fann *ann, struct fann_train_data *data);

}

namespace fann_pfx
{
	
	
	
	//----------------------------------------------
	// parallel versions of FANN functions
	//----------------------------------------------
	float fann_train_epoch_irpropm_parallel(struct fann *ann, struct fann_train_data *data);
	
	float fann_train_epoch_parallel(struct fann *ann, struct fann_train_data *data);
	
	void fann_train_on_data_parallel(struct fann *ann, struct fann_train_data *data,
									 unsigned int max_epochs,
									 unsigned int epochs_between_reports,
									 float desired_error);
	

}

#endif /* fann_train_pfx_h */

/*
 * AnnWrap.h
 *
 *  Created on: Jan 5, 2017
 *      Author: isivkov
 */

#ifndef _ANNWRAP_H_
#define _ANNWRAP_H_

extern "C"
{
#include <doublefann.h>
}

namespace fann_pfx
{

//----------------------------------------------
// class helper for OMP reduction
//----------------------------------------------
class AnnTrainWrap
{
protected:
	fann* _ann{nullptr};

	bool _isCopied{false};

public:
	AnnTrainWrap(){}

	AnnTrainWrap(fann* ann);

	void wrap(fann* ann);

	virtual ~AnnTrainWrap();

	virtual void prepare();

	virtual void copyFrom(AnnTrainWrap* annw);

	virtual void mergeWith(AnnTrainWrap* annw);

	virtual void trainIteration(fann_type* inputData, fann_type* outputData){};

	virtual void updateWeights(){};

	fann* getANN();

	void setANN(fann* ann);
};


//----------------------------------------------
// init function for omp reduction
//----------------------------------------------
inline void init_copy_ann(AnnTrainWrap& ann_priv, AnnTrainWrap& ann_orig)
{
	ann_priv = AnnTrainWrap();
	ann_priv.copyFrom(&ann_orig);
}

//----------------------------------------------
// copy function for omp reduction finalizing
//----------------------------------------------
inline void add_copy_to_orig(AnnTrainWrap& ann_priv, AnnTrainWrap& ann_orig)
{
	ann_orig.mergeWith(&ann_priv);
}

//----------------------------------------------
// omp reduction command
//----------------------------------------------
#pragma omp declare reduction( + : AnnTrainWrap : add_copy_to_orig(omp_in, omp_out))  initializer( init_copy_ann(omp_priv, omp_orig) )



}



#endif /* _ANNWRAP_H_ */

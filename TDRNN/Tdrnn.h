//
//  Tdrnn.h
//  test_fann
//
//  Created by Ilia Sivkov on 31/10/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#ifndef Tdrnn_h
#define Tdrnn_h

#include <cstring>

#include <vector>
#include <initializer_list>
#include <string>
#include <iostream>



#include "fann_train_pfx.h"
#include "Ann.h"

//extern "C"
//{
//#include <doublefann.h>
//}

namespace fann_pfx
{
//enum class ANN_Type
//{
//	standart=0,
//			array,
//			sparse
//};


class Tdrnn : public Ann
{
private:
//	fann *ann;
//
//	bool _isInitialized{false};

	int _numTimeDelayedInputs{1};
	int _numTimeDelayedRecurrentOutputs{0};
	int _numControlParams{0};

	std::vector<fann_type> _fullInput;

	std::vector<unsigned int> _layerSizes;

	void shift_time()
	{
		for(int i = 0; i < _numTimeDelayedInputs + _numTimeDelayedRecurrentOutputs - 1; i++)
		{
			_fullInput[i] = _fullInput[i+1];
		}
		// here full_input[num_td_inputs-1] and full_input[num_td_inputs + num_tdr_outputs - 1] are invalid
	}

public:

	Tdrnn (int num_time_delayed_inputs,
			int num_time_delayed_recurrent_outputs,
			int num_control_params,
			std::initializer_list<unsigned int> hidden_layers_sizes={});

	Tdrnn (int num_time_delayed_inputs,
			int num_time_delayed_recurrent_outputs,
			int num_control_params,
			std::initializer_list<unsigned int> hidden_layers_sizes,
			std::vector<LayerConnectionInfo> layersConnectionInfo);


	Tdrnn(int num_time_delayed,
			int num_control_params,
			std::initializer_list<unsigned int> hidden_layers_sizes={});


	Tdrnn(int num_time_delayed,
			std::initializer_list<unsigned int> hidden_layers_sizes={});


	Tdrnn(std::string fname,
			int num_time_delayed_inputs,
			int num_time_delayed_recurrent_outputs,
			int num_control_params);


	Tdrnn(std::string fname,
			int num_time_delayed,
			int _num_control_params=0);

	Tdrnn(){}

	virtual ~Tdrnn();

//	//--------------------------------------------------------------------------------
//	// These methods are wrappers for FANN functions
//	//--------------------------------------------------------------------------------
//	void set_activation_steepness_hidden(fann_type steepness);
//
//	void set_activation_steepness_output(fann_type steepness);
//
//	void set_activation_function_hidden(fann_activationfunc_enum activation_function);
//
//	void set_activation_function_output(fann_activationfunc_enum activation_function);
//
//	void set_train_stop_function(fann_stopfunc_enum train_stop_function);
//
//	void set_bit_fail_limit(fann_type bit_fail_limit);
//
//	void set_training_algorithm(fann_train_enum training_algorithm);
//
//	void init_weights(fann_train_data* data);
//
//	void train_on_data(fann_train_data* data,
//			unsigned int max_epochs,
//			unsigned int epochs_between_reports,
//			fann_type desired_error);
//
//	void train_on_data_parallel(fann_train_data* data,
//			unsigned int max_epochs,
//			unsigned int epochs_between_reports,
//			fann_type desired_error);
//
//	fann_type* run(fann_type* input);
//
//	float test_data(fann_train_data* data);
//
//	int save(const char *configuration_file);

	//--------------------------------------------------------------------------------
	// own get/set methods
	//--------------------------------------------------------------------------------
	void setControlParams(std::vector<fann_type> &controlParams);

	fann_type recurrentRun(fann_type input);

	std::vector<fann_type> processData(std::vector<fann_type>& data, std::vector<fann_type> controlParams);

	std::vector<fann_type> processData(std::vector<fann_type>& data);

//	fann* getANN();

	int getNumControlParams();

	int getFullNumInputs();

	int getNumTimeDelayedInputs();

	int getNumTimeDelayedRecurrentOutputs();

	void clearInput();
};
}
#endif /* Tdrnn_h */

//
//  AudioTrainData.hpp
//  test_fann
//
//  Created by Ilia Sivkov on 09/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#ifndef AudioTrainData_h
#define AudioTrainData_h

#include <stdio.h>
#include <string>
#include <vector>
#include <memory>

#include <sndfile.h>
#include <sndfile.hh>

#include "json.hpp"
using json = nlohmann::json;

namespace fann_pfx
{
	
	struct load_train_signal_info_t
	{
		std::string out_signal_fname;
		std::vector<double> control_params;
	};
	
	struct train_signal_data_t
	{
		std::vector<double> data;
		std::vector<double> control_params;
	};
	
	struct train_data_piece_info_t
	{
		int signalIdx;
		int dataOffset;
		int size;
	};
	
	class AudioTrainData
	{
	private:
		// signal data
		std::unique_ptr<std::vector<double>> _cleanSignal;

		std::unique_ptr<std::vector<train_signal_data_t>> _trainSignalSamples;
		
		std::vector<train_data_piece_info_t> _piecesInfo;

		int _numControlParams;
		
		bool _isLoaded{false};
		
	public:
		AudioTrainData(){};

		AudioTrainData(std::string metaDataFile);
		
		AudioTrainData(std::vector<load_train_signal_info_t> _trainSignals,
				std::string cleanSignalFName, int numControlParams);

		std::vector<double> loadWAV(std::string fileName);
		
		std::vector<double> loadWAV(SndfileHandle& file);

		void saveWAV(std::string fileName, std::vector<double>& data, int sampleRate );

		void loadTrainData(std::vector<load_train_signal_info_t> outSamplesFName,
				std::string cleanSignalFName, int numControlParams);
		
		int getNumControlParams();
		
		const std::vector<double>& getCleanSignal();
		
		const std::vector<train_signal_data_t>& getTrainSamples();
		
		std::vector<train_data_piece_info_t> getPiecesInfo();

		void splitSamplesByPieces(int numPieces, int startPiece, int endPiece);

		void setTrainSamples(std::unique_ptr<std::vector<train_signal_data_t>> trainSamples,
				std::unique_ptr<std::vector<double>> cleanSignal);

		bool isLoaded();
	};
}
#endif /* AudioTrainData_h */

//
//  AudioTrainData.cpp
//  test_fann
//
//  Created by Ilia Sivkov on 09/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#include <stdexcept>
#include <fstream>
#include <iostream>

#include "AudioTrainData.h"


namespace fann_pfx
{
	AudioTrainData::AudioTrainData(std::string metaDataFile)
	{
		json parser;
		std::ifstream(metaDataFile) >> parser;
	}
	
	
	
	AudioTrainData::AudioTrainData(std::vector<load_train_signal_info_t> outSamplesFName,
			std::string cleanSignalFName, int numControlParams)
	{
		loadTrainData(outSamplesFName,cleanSignalFName,numControlParams);
	}
	
	
	int AudioTrainData::getNumControlParams()
	{
		return _numControlParams;
	}
	
	const std::vector<double>& AudioTrainData::getCleanSignal()
	{
		return *_cleanSignal.get();
	}
	
	const std::vector<train_signal_data_t>& AudioTrainData::getTrainSamples()
	{
		return *_trainSignalSamples.get();
	}
	
	bool AudioTrainData::isLoaded()
	{
		return _isLoaded;
	}
	
	void AudioTrainData::loadTrainData(std::vector<load_train_signal_info_t> outSamplesFName,
			std::string cleanSignalFName, int numControlParams)
	{
		_numControlParams = numControlParams;
		
		_cleanSignal = std::unique_ptr<std::vector<double>> (new std::vector<double>(loadWAV(cleanSignalFName)) );

		_trainSignalSamples = std::unique_ptr<std::vector<train_signal_data_t>>(new std::vector<train_signal_data_t>);
		
		for (auto info : outSamplesFName)
		{
			train_signal_data_t train_data;
			
			if (info.control_params.size() != _numControlParams)
			{
				throw std::runtime_error( "Mismatch of num_control_params");
			}
			
			train_data.control_params = info.control_params;
			train_data.data = loadWAV(info.out_signal_fname);
			
			if(train_data.data.size() != _cleanSignal->size())
			{
				throw std::runtime_error( "Clean signal and processed signals have different lengths");
			}
			
			_trainSignalSamples->push_back(std::move(train_data));
		}
		
		splitSamplesByPieces(1,0,0);

		_isLoaded = true;
	}
	
	std::vector<double> AudioTrainData::loadWAV(SndfileHandle& file)
	{
		std::vector<double> data;

		data.resize(file.frames() * file.channels());

		file.read(data.data(), data.size());

		std::vector<double> signal0(file.frames());

		for(int i=0; i < file.frames(); i++)
		{
			signal0[i] = data[ i * file.channels() ];
		}

		return std::move(signal0);
	}

	std::vector<double> AudioTrainData::loadWAV(std::string fileName)
	{
		SndfileHandle file( fileName.c_str() );

		return std::move(loadWAV(file));
	}


	void AudioTrainData::saveWAV(std::string fileName, std::vector<double>& data, int sampleRate )
	{
		SndfileHandle file( fileName.c_str(), SFM_WRITE, SF_FORMAT_WAV | SF_FORMAT_PCM_24, 1, sampleRate );

		file.write( data.data(), data.size() );
	}


	void AudioTrainData::splitSamplesByPieces(int numPieces, int startPiece, int endPiece)
	{
		int pieceSize = _cleanSignal->size() / numPieces;

		_piecesInfo.clear();

		if( startPiece >= numPieces || startPiece > endPiece || endPiece >= numPieces)
		{
			std::runtime_error("Wrong number of pieces specified");
		}

		std::cout<<_cleanSignal->size()<<std::endl;
		for(int is = 0; is < _trainSignalSamples->size(); is++)
		{
			for(int ip = startPiece; ip <= endPiece; ip++)
			{
				train_data_piece_info_t piece;

				piece.signalIdx 	= is;
				piece.dataOffset 	= ip * pieceSize;

				if( pieceSize*( ip + 2 ) <= _cleanSignal->size() )
				{
					piece.size = pieceSize;
				}
				else
				{
					piece.size = _cleanSignal->size() - ip * pieceSize;
				}

				_piecesInfo.push_back(std::move(piece));

				std::cout<< piece.signalIdx<<" " << piece.dataOffset << " " << piece.size<< std::endl;
			}
		}
	}



	std::vector<train_data_piece_info_t> AudioTrainData::getPiecesInfo()
	{
		return _piecesInfo;
	}

	void AudioTrainData::setTrainSamples(std::unique_ptr<std::vector<train_signal_data_t>> trainSamples,
			std::unique_ptr<std::vector<double>> cleanSignal)
	{
		_trainSignalSamples = std::move( trainSamples );
		_cleanSignal = std::move( cleanSignal );
		splitSamplesByPieces(1,0,0);
	}
}

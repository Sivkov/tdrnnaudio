/*
 * Ann.h
 *
 *  Created on: Jan 19, 2017
 *      Author: isivkov
 */

#ifndef _ANN_H_
#define _ANN_H_

#include <cstring>

#include <vector>
#include <initializer_list>
#include <string>
#include <iostream>



#include "fann_train_pfx.h"

#include "LayerConnectionInfo.h"

extern "C"
{
#include <doublefann.h>
}

namespace fann_pfx
{

enum class ANN_Type
{
	standart=0,
	array,
	sparse
};



// wrapper for FANN ann
class Ann
{
private:
	fann *_ann;

	//bool _isInitialized{false};

	//std::vector<unsigned int> _layerSizes;

	void allocateBase(std::vector<unsigned int> layersSizes);


	template<typename ConnFuncT, typename NodeFuncT>
	int iterateOverConnections(std::vector<unsigned int> layersSizes,
			std::vector<LayerConnectionInfo> layersConnectionInfo,
			ConnFuncT connFunc, NodeFuncT nodeFunc)
	{
		int connectionCounter = 0;

		for( int i = 0; i < layersConnectionInfo.size(); i++ )
		{
			auto& conInfo = layersConnectionInfo[i];

			switch( conInfo.cType )
			{
				case ConnectionType::Full:
				{
					for( int inode = 0; inode < layersSizes[i+1]; inode++)
					{
						int startCon = connectionCounter;

						for( int inodePrev = 0; inodePrev < layersSizes[i]; inodePrev++)
						{
							// process connection between the specified nodes indices
							// i+1       -> idx of the current layer
							// inode     -> idx of the "ingoing" connecting node in the current layer
							// inodePrev -> idx of the "outgoing" connecting node in the previous layer
							connFunc(i+1, inode, inodePrev, connectionCounter);
							connectionCounter++;
						}

						// process connection between the specified node idx and a bias-node idx
						// layerSizes[i] -> bias node idx is layerSize + 1
						// real number of nodes in the layer MUST be layerSize + 1
						connFunc(i+1, inode, (int)layersSizes[i], connectionCounter);

						connectionCounter++;

						nodeFunc(i+1, inode, startCon, connectionCounter);
					}
				}break;

				case ConnectionType::Groupped:
				{
					for( int inode = 0; inode < layersSizes[i+1]; inode++)
					{
						int prevLayerStartNodeIdx = inode * conInfo.stepToGroup;

						int startCon = connectionCounter;

						if( prevLayerStartNodeIdx >= layersSizes[i] ) { break; }

						for( int icon = 0; icon < conInfo.numConnsInGroup; icon++ )
						{
							int prevLayerNodeToConnectIdx = prevLayerStartNodeIdx + icon * conInfo.stepToNodeInGroup;

							if( prevLayerNodeToConnectIdx >= layersSizes[i] ) { break; }

							// process connection between the specified nodes indices
							// i+1                       -> idx of the current layer
							// inode                     -> idx of the "ingoing" connecting node in the current layer
							// prevLayerNodeToConnectIdx -> idx of the "outgoing" connecting node in the previous layer
							connFunc(i+1, inode, prevLayerNodeToConnectIdx, connectionCounter);
							connectionCounter++;
						}

						// process connection between the specified node idx and a bias-node idx
						// layerSizes[i] -> bias node idx is layerSize + 1
						// real number of nodes in the layer MUST be layerSize + 1
						connFunc(i+1, inode, (int)layersSizes[i], connectionCounter);

						connectionCounter++;

						nodeFunc(i+1, inode, startCon, connectionCounter);
					}
				}break;

				default: std::runtime_error( "Error: Unknown case"); break;
			}
		}

		return connectionCounter;
	}

public:

	Ann(std::initializer_list<unsigned int> layersSizes);

	Ann(std::string fname);

	Ann(std::vector<unsigned int> layersSizes, std::vector<LayerConnectionInfo> layersConnectionInfo);

	Ann(){};

	virtual ~Ann();

	//--------------------------------------------------------------------------------
	// own get/set methods
	//--------------------------------------------------------------------------------
	fann* getANN();

	// initializers
	void create(std::vector<unsigned int> layerSizes);

	void create(std::string fname);

	void create(std::vector<unsigned int> layersSizes, std::vector<LayerConnectionInfo> layersConnectionInfo);

	//--------------------------------------------------------------------------------
	// These methods are wrappers for FANN functions
	//--------------------------------------------------------------------------------
	void set_activation_steepness_hidden(fann_type steepness);

	void set_activation_steepness_output(fann_type steepness);

	void set_activation_function_hidden(fann_activationfunc_enum activation_function);

	void set_activation_function_output(fann_activationfunc_enum activation_function);

	void set_train_stop_function(fann_stopfunc_enum train_stop_function);

	void set_bit_fail_limit(fann_type bit_fail_limit);

	void set_training_algorithm(fann_train_enum training_algorithm);

	void init_weights(fann_train_data* data);

	void print_connections();

	void train_on_data(fann_train_data* data,
			unsigned int max_epochs,
			unsigned int epochs_between_reports,
			fann_type desired_error);

	void train_on_data_parallel(fann_train_data* data,
			unsigned int max_epochs,
			unsigned int epochs_between_reports,
			fann_type desired_error);

	fann_type* run(fann_type* input);

	float test_data(fann_train_data* data);

	int save(const char *configuration_file);

	double get_MSE();

};

}



#endif /* _ANN_H_ */

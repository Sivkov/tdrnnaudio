/*
 * LayerConnectionInfo.h
 *
 *  Created on: May 28, 2017
 *      Author: isivkov
 */

#ifndef _ANNINFO_H_
#define _ANNINFO_H_

#include <vector>

extern "C"
{
#include <doublefann.h>
}

namespace fann_pfx
{
enum class ConnectionType
{
	Full,
	Groupped
};

// struct describes the INGOING connections to the layer
struct LayerConnectionInfo
{
	ConnectionType cType;
	int stepToGroup;
	int stepToNodeInGroup;
	int numConnsInGroup;
	fann_activationfunc_enum activationFuncType;
	fann_type activationStepness;
};


} /* namespace fann_pfx */

#endif /* _ANNINFO_H_ */

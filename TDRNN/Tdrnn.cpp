//
//  Tdrnn.cpp
//  test_fann
//
//  Created by Ilia Sivkov on 17/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#include "Tdrnn.h"

namespace fann_pfx
{


Tdrnn::Tdrnn (int num_time_delayed_inputs,
		int num_time_delayed_recurrent_outputs,
		int num_control_params,
		std::initializer_list<unsigned int> hidden_layers_sizes):
						_numTimeDelayedInputs(num_time_delayed_inputs),
						_numTimeDelayedRecurrentOutputs(num_time_delayed_recurrent_outputs),
						_numControlParams(num_control_params)
{
	_layerSizes = std::vector<unsigned int>(hidden_layers_sizes);

	_fullInput.resize(_numTimeDelayedInputs + _numTimeDelayedRecurrentOutputs + _numControlParams, 0);

	// add size of the 1st layer
	_layerSizes.insert(_layerSizes.begin(), _fullInput.size());

	// add size of the last layer (now its 1)
	_layerSizes.push_back(1);

//	for(auto a: _layerSizes) std::cout<<a<<" ";
//	std::cout<<std::endl;

	create(_layerSizes);
	//ann = fann_create_standard_array(static_cast<unsigned int>(_layerSizes.size()), _layerSizes.data());
};

Tdrnn::Tdrnn (int num_time_delayed_inputs,
		int num_time_delayed_recurrent_outputs,
		int num_control_params,
		std::initializer_list<unsigned int> hidden_layers_sizes,
		std::vector<LayerConnectionInfo> layersConnectionInfo):
		_numTimeDelayedInputs(num_time_delayed_inputs),
		_numTimeDelayedRecurrentOutputs(num_time_delayed_recurrent_outputs),
		_numControlParams(num_control_params)
{
	_layerSizes = std::vector<unsigned int>(hidden_layers_sizes);

	_fullInput.resize(_numTimeDelayedInputs + _numTimeDelayedRecurrentOutputs + _numControlParams, 0);

	// add size of the 1st layer
	_layerSizes.insert(_layerSizes.begin(), _fullInput.size());

	std::cout<<"Layer sizes: "<<_fullInput.size()<<std::endl;
	for(auto a: _layerSizes) std::cout<<a<<" ";
	std::cout<<std::endl;

	// add size of the last layer (now its 1)
	_layerSizes.push_back(1);

	create(_layerSizes, layersConnectionInfo);
}

Tdrnn::Tdrnn(int num_time_delayed, int num_control_params, std::initializer_list<unsigned int> hidden_layers_sizes):
					Tdrnn(num_time_delayed, num_time_delayed - 1, num_control_params, hidden_layers_sizes)
{ }


Tdrnn::Tdrnn(int num_time_delayed, std::initializer_list<unsigned int> hidden_layers_sizes):
					Tdrnn(num_time_delayed, num_time_delayed - 1, 0,  hidden_layers_sizes)
{ }

// temp constructor
// TODO remove num inputs
Tdrnn::Tdrnn(std::string fname, int num_time_delayed_inputs, int num_time_delayed_recurrent_outputs, int num_control_params):
					_numTimeDelayedInputs(num_time_delayed_inputs),
					_numTimeDelayedRecurrentOutputs(num_time_delayed_recurrent_outputs),
					_numControlParams(num_control_params)
{
	create(fname);

	_fullInput.resize(_numTimeDelayedInputs + _numTimeDelayedRecurrentOutputs + _numControlParams, 0);
}


Tdrnn::Tdrnn(std::string fname,int num_time_delayed, int num_control_params ):
					Tdrnn(fname, num_time_delayed, num_time_delayed - 1, num_control_params)
{		}


Tdrnn::~Tdrnn()
{
}

//--------------------------------------------------------------------------------
// These methods are wrappers for FANN functions
//--------------------------------------------------------------------------------
//void Tdrnn::set_activation_steepness_hidden(fann_type steepness)
//{
//	fann_set_activation_steepness_hidden(ann, steepness);
//}
//
//void Tdrnn::set_activation_steepness_output(fann_type steepness)
//{
//	fann_set_activation_steepness_output(ann, steepness);
//}
//
//void Tdrnn::set_activation_function_hidden(fann_activationfunc_enum activation_function)
//{
//	fann_set_activation_function_hidden(ann, activation_function);
//}
//
////--------------------------------------------------------------------------------
//void Tdrnn::set_activation_function_output(fann_activationfunc_enum activation_function)
//{
//	fann_set_activation_function_output(ann, activation_function);
//}
//
//
////--------------------------------------------------------------------------------
//void Tdrnn::set_train_stop_function(fann_stopfunc_enum train_stop_function)
//{
//	fann_set_train_stop_function(ann, train_stop_function);
//}
//
//
////--------------------------------------------------------------------------------
//void Tdrnn::set_bit_fail_limit(fann_type bit_fail_limit)
//{
//	fann_set_bit_fail_limit(ann, bit_fail_limit);
//}
//
//
////--------------------------------------------------------------------------------
//void Tdrnn::set_training_algorithm(fann_train_enum training_algorithm)
//{
//	fann_set_training_algorithm(ann, training_algorithm);
//}
//
//
////--------------------------------------------------------------------------------
//void Tdrnn::init_weights(fann_train_data* data)
//{
//	fann_init_weights(ann, data);
//}
//
//
////--------------------------------------------------------------------------------
//void Tdrnn::train_on_data(fann_train_data* data,
//		unsigned int max_epochs,
//		unsigned int epochs_between_reports,
//		fann_type desired_error)
//{
//	fann_train_on_data(ann, data, max_epochs, epochs_between_reports, desired_error);
//}
//
//
////--------------------------------------------------------------------------------
//void Tdrnn::train_on_data_parallel(fann_train_data* data,
//		unsigned int max_epochs,
//		unsigned int epochs_between_reports,
//		fann_type desired_error)
//{
//	fann_train_on_data_parallel(ann, data, max_epochs, epochs_between_reports, desired_error);
//}
//
////--------------------------------------------------------------------------------
//fann_type* Tdrnn::run(fann_type* input)
//{
//	return fann_run(ann, input);
//}
//
//
////--------------------------------------------------------------------------------
//float Tdrnn::test_data(fann_train_data* data)
//{
//	return fann_test_data(ann, data);
//}
//
//
////--------------------------------------------------------------------------------
//int Tdrnn::save(const char *configuration_file)
//{
//	return fann_save(ann, configuration_file);
//}

//--------------------------------------------------------------------------------
// own  methods
//--------------------------------------------------------------------------------
void Tdrnn::setControlParams(std::vector<fann_type> &controlParams)
{
	if( _numControlParams == 0 ) return;

	memcpy( &_fullInput[_numTimeDelayedInputs + _numTimeDelayedRecurrentOutputs],
			controlParams.data(), _numControlParams * sizeof(fann_type) );
}


//--------------------------------------------------------------------------------
fann_type Tdrnn::recurrentRun(fann_type input)
{
	_fullInput[ _numTimeDelayedInputs - 1 ] = input;

//	for(auto a: _fullInput) std::cout<< a<< " ";
//	std::cout<<std::endl;

	//std::cin.get();

	fann_type* output =  run(_fullInput.data()); // should be 1 in Tdrnn

	shift_time();

	_fullInput[ _numTimeDelayedInputs + _numTimeDelayedRecurrentOutputs- 1 ] = output[0];


	return output[0];
}


//--------------------------------------------------------------------------------
std::vector<fann_type> Tdrnn::processData(std::vector<fann_type>& data, std::vector<fann_type> controlParams)
{
	clearInput();

	setControlParams(controlParams);

	std::vector<fann_type> processed_data( data.size() );

	for( int i = 0; i < data.size(); i++ )
	{
		processed_data[i] = recurrentRun( data[i] );
	}

	return std::move(processed_data);
}


//--------------------------------------------------------------------------------
std::vector<fann_type> Tdrnn::processData(std::vector<fann_type>& data)
{
	std::vector<fann_type> cp={};

	return std::move(processData(data, cp));
}

//--------------------------------------------------------------------------------
//fann* Tdrnn::getANN()
//{
//	return ann;
//}


//--------------------------------------------------------------------------------
int Tdrnn::getNumControlParams()
{
	return _numControlParams;
}


//--------------------------------------------------------------------------------
int Tdrnn::getFullNumInputs()
{
	return _fullInput.size();
}


//--------------------------------------------------------------------------------
int Tdrnn::getNumTimeDelayedInputs()
{
	return _numTimeDelayedInputs;
}


//--------------------------------------------------------------------------------
int Tdrnn::getNumTimeDelayedRecurrentOutputs()
{
	return _numTimeDelayedRecurrentOutputs;
}


//--------------------------------------------------------------------------------
void Tdrnn::clearInput()
{
	memset(_fullInput.data(), 0, sizeof(fann_type) * _fullInput.size());
}
}

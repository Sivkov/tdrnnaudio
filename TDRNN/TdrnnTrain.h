//
//  Tdrnn_Train.h
//  test_fann
//
//  Created by Ilia Sivkov on 15/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//

#ifndef Tdrnn_Train_h
#define Tdrnn_Train_h

#include "AudioTrainData.h"
#include "Tdrnn.h"

namespace fann_pfx
{
	class TdrnnTrain
	{
	private:
		Tdrnn* _tdrnn;
		AudioTrainData* _trainData;
		int _forwardShift{0};
		
		
	public:
		TdrnnTrain(Tdrnn* tdrnn, AudioTrainData* trainData);
		
		virtual ~TdrnnTrain(){}

		void setForwardShift(int forwardShift);

		//Initialize Tdrnn weights using Widrow + Nguyen's algorithm
		virtual void initWeights(double dataSpread = 0.6);
		
		void train(unsigned int max_epochs,
				unsigned int epochs_between_reports,
				float desired_error);

		void train(unsigned int max_epochs,
				unsigned int epochs_between_reports,
				float desired_error,
				std::vector<double>& procData,
				std::vector<double>& cleanData,
				int start, int end,
				std::vector<double> controlParams={});

		void train(unsigned int max_epochs,
				unsigned int epochs_between_reports,
				float desired_error,
				std::vector<double>& procData,
				std::vector<double>& cleanData);

		double trainEpoch();

		double trainEpochOnExtData(std::vector<double>& procData,
				std::vector<double>& cleanData,
				int start, int end,
				std::vector<double> controlParams={});

		double trainEpochOnExtData(std::vector<double>& procData,
				std::vector<double>& cleanData);
	};
}

#endif /* Tdrnn_Train_h */

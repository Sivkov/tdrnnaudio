/*
 * AnnWrapRProp.h
 *
 *  Created on: Jan 5, 2017
 *      Author: isivkov
 */

#ifndef _ANNWRAPRPROP_H_
#define _ANNWRAPRPROP_H_

#include "AnnTrainWrap.h"

namespace fann_pfx
{

class AnnWrapRProp : public AnnTrainWrap
{
public:

	AnnWrapRProp(): AnnTrainWrap() {}

	AnnWrapRProp(fann* ann): AnnTrainWrap(ann) {}

	virtual void trainIteration(fann_type* inputData, fann_type* outputData)
	{
		fann_run(_ann, inputData);
		fann_compute_MSE(_ann, outputData );
		fann_backpropagate_MSE(_ann);
		fann_update_slopes_batch(_ann, _ann->first_layer + 1, _ann->last_layer - 1);
	}

	virtual void updateWeights()
	{
		fann_type *train_slopes = _ann->train_slopes;
		fann_type *weights = _ann->weights;
		fann_type *prev_steps = _ann->prev_steps;
		fann_type *prev_train_slopes = _ann->prev_train_slopes;

		fann_type prev_step, slope, prev_slope, next_step, same_sign;

		float increase_factor = _ann->rprop_increase_factor;	/*1.2; */
		float decrease_factor = _ann->rprop_decrease_factor;	/*0.5; */
		float delta_min = _ann->rprop_delta_min;	/*0.0; */
		float delta_max = _ann->rprop_delta_max;	/*50.0; */

		#pragma omp parallel for
		for(unsigned int i = 0; i < _ann->total_connections; i++)
		{
			prev_step = fann_max(prev_steps[i], (fann_type) 0.0001);	/* prev_step may not be zero because then the training will stop */
			slope = train_slopes[i];
			prev_slope = prev_train_slopes[i];

			same_sign = prev_slope * slope;

			if(same_sign >= 0.0)
				next_step = fann_min(prev_step * increase_factor, delta_max);
			else
			{
				next_step = fann_max(prev_step * decrease_factor, delta_min);
				slope = 0;
			}

			if(slope < 0)
			{
				weights[i] -= next_step;
				if(weights[i] < -1500)
					weights[i] = -1500;
			}
			else
			{
				weights[i] += next_step;
				if(weights[i] > 1500)
					weights[i] = 1500;
			}

			/*if(i == 2){
			 * printf("weight=%f, slope=%f, next_step=%f, prev_step=%f\n", weights[i], slope, next_step, prev_step);
			 * } */

			/* update global data arrays */
			prev_steps[i] = next_step;
			prev_train_slopes[i] = slope;
			train_slopes[i] = 0.0;
		}
	}
};

//----------------------------------------------
// init function for omp reduction
//----------------------------------------------
inline void init_copy_ann(AnnWrapRProp& ann_priv, AnnWrapRProp& ann_orig)
{
	ann_priv = AnnWrapRProp();
	ann_priv.copyFrom(&ann_orig);
}

//----------------------------------------------
// copy function for omp reduction finalizing
//----------------------------------------------
inline void add_copy_to_orig(AnnWrapRProp& ann_priv, AnnWrapRProp& ann_orig)
{
	ann_orig.mergeWith(&ann_priv);
}

//----------------------------------------------
// omp reduction command
//----------------------------------------------
#pragma omp declare reduction( + : AnnWrapRProp : add_copy_to_orig(omp_in, omp_out))  initializer( init_copy_ann(omp_priv, omp_orig) )


}


#endif /* _ANNWRAPRPROP_H_ */

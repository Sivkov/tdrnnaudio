
#include "../TDRNN/Tdrnn.h"
#include "../TDRNN/AudioTrainData.h"
#include "../TDRNN/TdrnnTrain.h"

#include <fstream>

using namespace fann_pfx;

void test_rnn(Tdrnn& tdrnn)
{
	AudioTrainData trainData;

	std::vector<double> wav_proc_dat = trainData.loadWAV("od_check1.wav");

	std::vector<double> wav_clean_dat = trainData.loadWAV("check_clean.wav");

	std::vector<double> wav_neuro_dat = tdrnn.processData(wav_clean_dat);

	double max_diff = 0.0;

	double sqdif=0.0;

	for(int i=0; i<20; i++)
	{
		std::cout<<wav_clean_dat[i]<<" "<<wav_proc_dat[i]<<" "<<wav_neuro_dat[i]<<std::endl;
	}

	std::ofstream outf("test_result.dat");



	for(int i=0; i<wav_neuro_dat.size(); i++)
	{
		double diff = wav_neuro_dat[i] - wav_proc_dat[i];
		sqdif += diff*diff;

		if( std::abs(diff) > max_diff )
		{
			max_diff = std::abs(diff);
		}

		outf<<wav_neuro_dat[i]<<std::endl;
	}

	outf.close();
	double meandiff = std::sqrt(sqdif)/(double)wav_neuro_dat.size();

	std::cout<<"MEAN ERROR: "<<meandiff<<std::endl;
	std::cout<<"MAX ERROR: "<<max_diff<<std::endl;
}

void train_rnn(Tdrnn& tdrnn)
{
	const float desired_error = (const float) 0;
	const unsigned int max_epochs = 2500;
	const unsigned int epochs_between_reports = 10;

	std::vector<unsigned int> lrsz(fann_get_num_layers(tdrnn.getANN()));

	fann_get_layer_array(tdrnn.getANN(),lrsz.data());

	std::cout<<"Layer sizes:"<<std::endl;
	for(auto a: lrsz) std::cout<<a<<" ";
	std::cout<<std::endl;

	tdrnn.set_activation_steepness_hidden( 1. );
	tdrnn.set_activation_steepness_output( 1. );

	tdrnn.set_activation_function_hidden(FANN_SIGMOID_SYMMETRIC);
	tdrnn.set_activation_function_output(FANN_SIGMOID_SYMMETRIC);

	tdrnn.set_train_stop_function(FANN_STOPFUNC_BIT);
	tdrnn.set_bit_fail_limit(0.001);

	tdrnn.set_training_algorithm(FANN_TRAIN_RPROP);


	std::vector<load_train_signal_info_t> outSamplesFName
	{
		//		{ "neuro_0-005.wav", {0.0} },
		//		{ "neuro_25-005.wav", {0.25} },
		//		{ "neuro_50-005.wav", {0.50} },
		{ "od_sample1.wav", {} },
		//		{ "neuro_100-005.wav", {1.0} },
	};

	std::string cleanSignalFName = "sample_clean.wav" ;

	AudioTrainData trainData(outSamplesFName, cleanSignalFName, tdrnn.getNumControlParams());

	TdrnnTrain trainer(&tdrnn, &trainData);

	trainer.initWeights(2.);

	printf("Training network 1.\n");

	trainData.splitSamplesByPieces(4,0,3);

	trainer.setForwardShift(1);

	trainer.train(max_epochs, epochs_between_reports, desired_error);

	printf("Saving network.\n");

	tdrnn.save("filter.net");
}

int main()
{
	int num_control_params = 0;

	Tdrnn tdrnn(31,0,num_control_params,{15,9});

	train_rnn(tdrnn);

	test_rnn(tdrnn);

	return 0;
}

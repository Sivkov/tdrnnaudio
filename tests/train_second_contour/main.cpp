
#include <cmath>
#include <fstream>

#include "../TDRNN/Tdrnn.h"
#include "../TDRNN/AudioTrainData.h"
#include "../TDRNN/TdrnnTrain.h"

using namespace fann_pfx;

int main()
{
	const float desired_error = (const float) 0;
	const unsigned int max_epochs = 10000;
	const unsigned int epochs_between_reports = 10;

	Tdrnn tdrnn("filter.net", 3, 0);

	std::vector<unsigned int> lrsz(fann_get_num_layers(tdrnn.getANN()));

	fann_get_layer_array(tdrnn.getANN(),lrsz.data());

	std::cout<<"Layer sizes:"<<std::endl;

	for(auto a: lrsz) std::cout<<a<<" ";
	std::cout<<std::endl;

	// create class to load data
	AudioTrainData trainData;

	// desired data
	std::vector<double> wav_proc_dat = trainData.loadWAV("sample_lp_initial.wav");

	std::vector<double> wav_clean_dat = trainData.loadWAV("sample_clear.wav");

	// get processed data to use it later as input
	std::vector<double> wav_neuro_dat = tdrnn.processData(wav_clean_dat);

	// test init ANN to be sure
	double max_diff = 0.0;

	double sqdif=0.0;

	for(int i=0; i<wav_neuro_dat.size(); i++)
	{
		double diff = wav_neuro_dat[i] - wav_proc_dat[i];
		sqdif += diff*diff;

		if( std::abs(diff) > max_diff )
		{
			max_diff = std::abs(diff);
		}

	}

	double meandiff = std::sqrt(sqdif)/(double)wav_neuro_dat.size();

	std::cout<<"Test initial ANN"<<std::endl;
	std::cout<<"MEAN ERROR: "<<meandiff<<std::endl;
	std::cout<<"MAX ERROR: "<<max_diff<<std::endl;

	// setup second ANN
	Tdrnn tdrnn2(5,0,{7});

	tdrnn2.set_activation_steepness_hidden( 1 );
	tdrnn2.set_activation_steepness_output( 1 );

	tdrnn2.set_activation_function_hidden(FANN_SIGMOID_SYMMETRIC);
	tdrnn2.set_activation_function_output(FANN_SIGMOID_SYMMETRIC);

	tdrnn2.set_train_stop_function(FANN_STOPFUNC_BIT);
	tdrnn2.set_bit_fail_limit(0.001);

	tdrnn2.set_training_algorithm(FANN_TRAIN_RPROP);

	// setup trainer
	TdrnnTrain trainer(&tdrnn2, &trainData);

	trainer.initWeights(1.);

	trainer.train(max_epochs, epochs_between_reports, desired_error, wav_proc_dat, wav_neuro_dat);

	std::vector<double> second_neuro_dat = tdrnn2.processData(wav_neuro_dat);

	// test second ANN
	max_diff = 0.0;

	sqdif=0.0;

	for(int i=0; i<wav_neuro_dat.size(); i++)
	{
		double diff = second_neuro_dat[i] - wav_proc_dat[i];
		sqdif += diff*diff;

		if( std::abs(diff) > max_diff )
		{
			max_diff = std::abs(diff);
		}

	}

	meandiff = std::sqrt(sqdif)/(double)wav_neuro_dat.size();

	std::cout<<"Test second ANN"<<std::endl;
	std::cout<<"MEAN ERROR: "<<meandiff<<std::endl;
	std::cout<<"MAX ERROR: "<<max_diff<<std::endl;
	return 0;
}

//
//  main.cpp
//  test_fann
//
//  Created by Ilia Sivkov on 04/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//
#include <iostream>
#include <fstream>
#include <iterator>
#include <cmath>
#include <Tdrnn.h>

using namespace std;
using namespace fann_pfx;


int main()
{
	std::vector<unsigned int> layersSizes = {10,8,6,1};

	std::vector<LayerConnectionInfo> layersConnectionInfo =
			{{ConnectionType::Groupped, 1, 1, 3, FANN_SIGMOID_STEPWISE, 0.5},
			{ConnectionType::Groupped, 1, 1, 3, FANN_SIGMOID_STEPWISE, 0.5},
			{ConnectionType::Full, 0, 0, 0, FANN_SIGMOID_STEPWISE, 0.5}};

	Tdrnn tdrnn;
	tdrnn.create(layersSizes, layersConnectionInfo);

	tdrnn.print_connections();
}

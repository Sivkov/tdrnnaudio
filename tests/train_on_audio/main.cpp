
#include "../TDRNN/Tdrnn.h"
#include "../TDRNN/AudioTrainData.h"
#include "../TDRNN/TdrnnTrain.h"

#include <fstream>
#include <algorithm>

using namespace fann_pfx;

void test_rnn(Tdrnn& tdrnn)
{
	AudioTrainData trainData;

	std::vector<double> wav_proc_dat = trainData.loadWAV("check_sample_proc.wav");

	SndfileHandle fclean("check_sample_clean.wav");

	std::vector<double> wav_clean_dat = trainData.loadWAV(fclean);

	std::vector<double> wav_neuro_dat = tdrnn.processData(wav_clean_dat);

	double max_diff = 0.0;

	double sqdif=0.0;

	for(int i=0; i<std::min(wav_clean_dat.size(),(size_t)20); i++)
	{
		std::cout<<wav_clean_dat[i]<<" "<<wav_proc_dat[i]<<" "<<wav_neuro_dat[i]<<std::endl;
	}

	trainData.saveWAV("sample_neuro_proc.wav", wav_neuro_dat, fclean.samplerate());

	// calc error
	for(int i=0; i<wav_neuro_dat.size(); i++)
	{
		double diff = wav_neuro_dat[i] - wav_proc_dat[i];
		sqdif += diff*diff;

		if( std::abs(diff) > max_diff )
		{
			max_diff = std::abs(diff);
		}

	}

	double meandiff = std::sqrt(sqdif)/(double)wav_neuro_dat.size();

	std::cout<<"MEAN ERROR: "<<meandiff<<std::endl;
	std::cout<<"MAX ERROR: "<<max_diff<<std::endl;
}

void train_rnn(Tdrnn& tdrnn)
{
	const float desired_error = (const float) 0;
	const unsigned int max_epochs = 10000;
	const unsigned int epochs_between_reports = 10;

	std::vector<unsigned int> lrsz(fann_get_num_layers(tdrnn.getANN()));

	fann_get_layer_array(tdrnn.getANN(),lrsz.data());

	std::cout<<"Layer sizes:"<<std::endl;
	for(auto a: lrsz) std::cout<<a<<" ";
	std::cout<<std::endl;

	tdrnn.set_activation_steepness_hidden( 1. );
	tdrnn.set_activation_steepness_output( 1. );

	tdrnn.set_activation_function_hidden(FANN_LINEAR);
	tdrnn.set_activation_function_output(FANN_LINEAR);

	tdrnn.set_train_stop_function(FANN_STOPFUNC_BIT);
	tdrnn.set_bit_fail_limit(0.001);

	tdrnn.set_training_algorithm(FANN_TRAIN_RPROP);


	std::vector<load_train_signal_info_t> outSamplesFName
	{
		//		{ "neuro_0-005.wav", {0.0} },
		//		{ "neuro_25-005.wav", {0.25} },
		//		{ "neuro_50-005.wav", {0.50} },
		{ "sample_lp_initial.wav", {} },
		//		{ "neuro_100-005.wav", {1.0} },
	};

	std::string cleanSignalFName = "sample_clear.wav" ;

	AudioTrainData trainData(outSamplesFName, cleanSignalFName, tdrnn.getNumControlParams());

	TdrnnTrain trainer(&tdrnn, &trainData);

	trainer.initWeights(1.);

	printf("Training network 1.\n");

	trainData.splitSamplesByPieces(4,0,3);

	trainer.setForwardShift(1);

	trainer.train(max_epochs, epochs_between_reports, desired_error);

	printf("Saving network.\n");

	tdrnn.print_connections();
	tdrnn.save("filter.net");

	printf("done.\n");
}

int main()
{
	int num_control_params = 0;

	Tdrnn tdrnn(3,num_control_params);

	train_rnn(tdrnn);

	test_rnn(tdrnn);

	return 0;
}

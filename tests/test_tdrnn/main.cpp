//
//  main.cpp
//  test_fann
//
//  Created by Ilia Sivkov on 04/12/16.
//  Copyright © 2016 PechenegFX. All rights reserved.
//
#include <iostream>
#include <fstream>
#include <iterator>
#include <cmath>
#include <Tdrnn.h>

using namespace std;

int main()
{
	ifstream testf("lp_test.dat");
	ifstream checkf("lp_check.dat");

	ofstream outf("test_result.dat");

	std::vector<fann_type> vec;
	std::vector<fann_type> results;
	std::vector<fann_type> check;

	std::copy(std::istream_iterator<double>(testf), std::istream_iterator<double>(), std::back_inserter(vec));
	std::copy(std::istream_iterator<double>(checkf), std::istream_iterator<double>(), std::back_inserter(check));

	testf.close();
	checkf.close();

	results.reserve(vec.size());

	fann_pfx::Tdrnn tdrnn("filter.net", 3);

	tdrnn.print_connections();

	fann_type sqdif=0.0;

	int size = std::min(vec.size(),check.size());

	for(int i=0; i<size; i++)
	{
		fann_type res = tdrnn.recurrentRun(vec[i]);

		results.push_back(res);

		fann_type diff = res - check[i];
		sqdif += diff*diff;

		outf<<res<<endl;
	}

	fann_type meandiff = std::sqrt(sqdif)/(fann_type)size;

	std::cout<<"TEST ERROR: "<<meandiff<<std::endl;


	outf.close();
	return 0;
}

#include <iostream>
#include <system_error>
#include <vector>
#include <memory>

#include "../TDRNN/Tdrnn.h"
#include "../TDRNN/AudioTrainData.h"

int main()
{
	fann_pfx::AudioTrainData tdata;
	
	std::vector<double> signal = tdata.loadWAV("neuro_clean-007.wav");
	
	std::cout<<"wave file read with size: "<<signal.size()<<" frames"<<std::endl;

	return 0;
}
